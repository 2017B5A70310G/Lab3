
class Room {
	
	private int breadth;
	private int height;
	private int length;
	
	Room(int length, int breadth, int height) {
		this.breadth = breadth;
		this.length = length;
		this.height = height;
	}
	
	public int getVolume() {
		return length*breadth*height;
	}
	
	public int getSurfaceArea() {
		return 2*(length*breadth + breadth*height + length*height);
	}
	
	public int calculateMaintenanceCost() {
		return 5*getVolume() + 2*getSurfaceArea();
	}
}
