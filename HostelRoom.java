
import java.util.*;

class HostelRoom extends Room {
	
	static int NUMBER_OF_APPLIANCES = 3;
	private boolean workingAppliances[];
	
	HostelRoom(int length, int breadth, int height) {
		super(length, breadth, height);
		workingAppliances = new boolean[NUMBER_OF_APPLIANCES];
	}
	
	public void setStateOfAppliances(String state) {
		StringTokenizer stringTokenizer = new StringTokenizer(state, ",");
		
		String workingState0 = stringTokenizer.nextToken();
		workingAppliances[0] = workingState0.equalsIgnoreCase("true");
		
		String workingState1 = stringTokenizer.nextToken();
		workingAppliances[1] = workingState1.equalsIgnoreCase("true");
		
		String workingState2 = stringTokenizer.nextToken();
		workingAppliances[2] = workingState2.equalsIgnoreCase("true");
	}
	
	public int calculateMaintenanceCost() {
		return super.calculateMaintenanceCost() + 5 * (workingAppliances[0] ? 0 : 1) + 3 * (workingAppliances[1] ? 0 : 1) + 2 * (workingAppliances[2] ? 0 : 1);
	}
}
