
class ClassRoom extends Room {
	private boolean areLightsOn;
	private boolean isAcOn;
	
	ClassRoom(int length, int breadth, int height) {
		super(length, breadth, height);
		areLightsOn = false;
		isAcOn = false;
	}
	
	public void flipApplianceState(String appliance) {
		if (appliance.equals("Lights")) areLightsOn = !areLightsOn;
		if (appliance.equals("AC")) isAcOn = !isAcOn;
	}
	
	public int calculateMaintenanceCost() {
		return super.calculateMaintenanceCost() + 10 * (isAcOn ? 1 : 0) + 5 * (areLightsOn ? 1 : 0);
	}
}
