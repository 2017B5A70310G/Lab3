
class AcadBlock extends CampusBuilding {
	
	private ClassRoom classRooms[];
	static int MAX_NUMBER_OF_CLASS_ROOMS = 200;
	
	AcadBlock(String name) {
		super(name);
		classRooms = new ClassRoom[MAX_NUMBER_OF_CLASS_ROOMS];
	}
	
	public boolean addClassRoom(ClassRoom classRoom) {
		if (numberOfRooms >= MAX_NUMBER_OF_CLASS_ROOMS) return false;
		
		classRooms[numberOfRooms++] = classRoom;
		return true;
	}
	
	public int calculateMaintenanceCost() {
	
		int totalCost = 0;
		for (int i = 0; i < numberOfRooms; i++) {
			totalCost += classRooms[i].calculateMaintenanceCost();
		}
		
		return totalCost;
	}
}
