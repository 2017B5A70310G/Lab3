
class Hostel extends CampusBuilding {

	private HostelRoom hostelRooms[];
	static int MAX_NUMBER_OF_HOSTEL_ROOMS = 100;
	
	Hostel(String name) {
		super(name);
		hostelRooms = new HostelRoom[MAX_NUMBER_OF_HOSTEL_ROOMS];
	}
	
	public boolean addHostelRoom(HostelRoom hostelRoom) {
		if (numberOfRooms >= MAX_NUMBER_OF_HOSTEL_ROOMS) return false;
		
		hostelRooms[numberOfRooms++] = hostelRoom;
		return true;
	}
	
	public int calculateMaintenanceCost() {
		
		int totalCost = 0;
		for (int i = 0; i < numberOfRooms; i++) {
			totalCost += hostelRooms[i].calculateMaintenanceCost();
		}
		
		
		return totalCost;
	}
}
