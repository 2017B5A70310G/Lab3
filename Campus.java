
class Campus {
	private AcadBlock acadBlock;
	private int hostelCount;
	private Hostel hostels[];
	static int MAX_NUMBER_OF_HOSTELS = 10;
	private static String name;
	
	Campus(String name, AcadBlock acadBlock) {
		this.name = name;
		this.acadBlock = acadBlock;
		hostels = new Hostel[MAX_NUMBER_OF_HOSTELS];
		hostelCount = 0;
	}
	
	public static String getName() {
		return name;
	}
	
	public boolean addHostel(String hostelName) {
		if (hostelCount >= MAX_NUMBER_OF_HOSTELS) return false;
		
		hostels[hostelCount++] = new Hostel(hostelName);
		return true;
	}
	
	public int calculateCampusMaintenance() {
		
		int totalCost = 0;
		for (int i = 0; i < hostelCount; i++) {
			totalCost += hostels[i].calculateMaintenanceCost();
		}
		
		totalCost += acadBlock.calculateMaintenanceCost();
		
		return totalCost;
	}
}
